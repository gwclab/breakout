﻿using UnityEngine;
using System.Collections;

public class Screen2 : MonoBehaviour {
    public GameObject[] prefab;
    private bool gameClear = false;     // クリアフラグ.
    public GUIStyle gui_gameClear;      // ゲームクリア用のGUIStyle.
    static private int stage_no = 1;

    // Use this for initialization
    void Start () {
        switch (stage_no)
        {
            case 1:
                blockSetting_01();      // ブロックの配置.
                break;
            case 2:
                blockSetting_02();      // ブロックの配置.
                break;
        }
    }

    //■■■ステージ１のブロックの配置■■■■■■■■■■■■■■■■■■■■■■■■■
    private void blockSetting_01()
    {
        for (int i = 0; i < 4; i++)
        {
            // プレハブの作成.
            GameObject block_right1 = GameObject.Instantiate(prefab[0]) as GameObject;
            GameObject block_right2 = GameObject.Instantiate(prefab[1]) as GameObject;
            GameObject block_right3 = GameObject.Instantiate(prefab[2]) as GameObject;
            GameObject block_left1 = GameObject.Instantiate(prefab[0]) as GameObject;
            GameObject block_left2 = GameObject.Instantiate(prefab[1]) as GameObject;
            GameObject block_left3 = GameObject.Instantiate(prefab[2]) as GameObject;
            // プレハブの配置.
            block_right1.transform.position = new Vector3(3.0f + (5 * i), 19.0f, 0.0f);
            block_right2.transform.position = new Vector3(3.0f + (5 * i), 21.0f, 0.0f);
            block_right3.transform.position = new Vector3(3.0f + (5 * i), 23.0f, 0.0f);
            block_left1.transform.position = new Vector3(-3.0f - (5 * i), 19.0f, 0.0f);
            block_left2.transform.position = new Vector3(-3.0f - (5 * i), 21.0f, 0.0f);
            block_left3.transform.position = new Vector3(-3.0f - (5 * i), 23.0f, 0.0f);
        }
    }

    //■■■ステージ２のブロックの配置■■■■■■■■■■■■■■■■■■■■■■■■■
    private void blockSetting_02()
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                // プレハブの作成.
                GameObject block_r = GameObject.Instantiate(prefab[i]) as GameObject;
                GameObject block_l = GameObject.Instantiate(prefab[i]) as GameObject;
                // プレハブの配置.
                block_r.transform.position = new Vector3(3.0f + (5 * i) + (2 * j), 17.0f + (2 * j), 0.0f);
                block_l.transform.position = new Vector3(-3.0f - (5 * i) - (2 * j), 17.0f + (2 * j), 0.0f);
            }
        }
    }

    //■■■クリア判定(ブロックが残っているかを確認する)■■■■■■■■■■■■■■■■■■■■■■■■■
    public bool checkBlockAll()
    {
        if ((GameObject.FindWithTag("block1") == null) && (GameObject.FindWithTag("block2") == null) && (GameObject.FindWithTag("block3") == null))
        {
            gameClear = true;                               // ブロックが見つからない場合、クリアフラグを立てる
        }
        return gameClear;
    }

    //■■■GUI■■■■■■■■■■■■■■■■■■■■■■■■■
    void OnGUI()
    {
        if (gameClear)
        {       // クリアフラグがたっていたら、ラベル表示.
            GUI.Label(new Rect(0, 0, Screen.width, Screen.height), "Stage Clear", gui_gameClear);
        }
    }

    void Update()
    {
        if (gameClear)
        {
            if (Input.GetMouseButtonDown(0))
            {
                stage_no++;
                if (stage_no < 3)
                {
                    Application.LoadLevel(stage_no);
                }
            }
        }
    }
}
