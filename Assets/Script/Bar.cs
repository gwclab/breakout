﻿using UnityEngine;
using System.Collections;

public class Bar : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (transform.position.x > -18.5f)
            {
                transform.position += Vector3.left * 10 * Time.deltaTime;
            }
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            if (transform.position.x < 18.5f)
            {
                transform.position += Vector3.right * 10 * Time.deltaTime;
            }
        }
    }

}
