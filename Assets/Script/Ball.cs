﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

    private Rigidbody rb;
    private AudioSource audioSource;        // AudioSorceを格納する変数の宣言.
    public AudioClip sound;             // 効果音を格納する変数の宣言.
    private Screen2 stageScene = null;

    // Use this for initialization
    void Start () {
        audioSource = gameObject.AddComponent<AudioSource>();    // AudioSorceコンポーネントを追加し、変数に代入.
        audioSource.clip = sound;       // 鳴らす音(変数)を格納.
        audioSource.loop = false;       // 音のループなし。
        stageScene = GameObject.Find("GameRoot").GetComponent<Screen2>();	// 『GameRoot』オブジェクトを探し、そのオブジェクトが持っているスクリプトを代入。

        rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(10.0f, 10.0f, 0.0f);
    }

    // Update is called once per frame
    void Update () {
	
	}

    void OnCollisionEnter(Collision other)
    {
        audioSource.Play();		// 音を鳴らす.

        Vector3 V = rb.velocity;   // 速度を取得.

        if (V.x < 0.0f)
        {       // Xの速度が負の値なら.
            V.x = -10.0f;        // Xの値を -5.0f に.
        }
        else
        {               // そうでないなら (Xの速度が0以上なら).
            V.x = 10.0f;         // Xの値を 5.0f に.
        }

        if (V.y < 0.0f)
        {       // Yの速度が負の値なら.
            V.y = -10.0f;        // Yの値を -5.0f に.
        }
        else
        {               // そうでないなら (Yの速度が0以上なら).
            V.y = 10.0f;         // Yの値を 5.0f に.
        }

        rb.velocity = V;   // 値を反映.

        if (stageScene.checkBlockAll())
        {       // ブロック確認 (関数を呼びだし、trueが返ってきたらカッコ内を実行)
            rb.velocity = Vector3.zero;    // ボールの速度をゼロにする.
        }
    }
}
