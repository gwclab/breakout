﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour {

    private Score c03_score;
    private int hp;
    public GameObject prefab;		// プレハブ用変数.

    void Start()
    {
        c03_score = GameObject.Find("GameRoot").GetComponent<Score>();

        switch (tag)
        {   // タグ名で分岐
            case "block2": hp = 2; break;          // 『Block02』のhpを2に設定. 
            default: hp = 1; break;         // それ以外　 のhpを1に設定. 
        }
    }

    void OnCollisionEnter(Collision other)
    {

        hp--;   // ボールが当たったら、hpを減らす.

        if (hp == 0)
        {
            switch (tag)
            {   // タグ名で分岐
                case "block1":
                    c03_score.Additional_score(1);
                    Debug.Log("１ポイント　ゲット！");
                    break;
                case "block2":
                    c03_score.Additional_score(2);
                    Debug.Log("２ポイント　ゲット！");
                    break;
                case "block3":
                    c03_score.Additional_score(3);
    				Instantiate(prefab , transform.position , Quaternion.identity);		// プレハブ設定.
                    break;
            }

            Destroy(gameObject);    // ブロックの破壊.
        }
        else
        {
            GetComponent<Renderer>().material.color *= new Color(1.0f, 1.0f, 1.0f, 0.4f);
        }
    }
}
