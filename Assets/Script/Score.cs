﻿using UnityEngine;
using System.Collections;


public class Score : MonoBehaviour
{

    static private int score = 0;          // スコア.
    public GUIStyle gui_score;      // GUIスタイル

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    // スコア加算用の関数.
    public void Additional_score(int value)
    {
        score += value;
    }

    // スコアの表示.
    void OnGUI()
    {
        GUI.Label(new Rect(0, 0, Screen.width - 10, 30), "SCORE : " + score, gui_score);
    }
}
